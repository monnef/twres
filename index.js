#!/usr/bin/env node
const Jimp = require('jimp');
const { max, min, subtract, divide, isEmpty, head, cloneDeep } = require('lodash/fp');
const { pipe, map, zip, uncurryTuple, flow, opt } = require('ts-opt');
const path = require('path');
const yargs = require('yargs');
const { hideBin } = require('yargs/helpers');
const { exit } = require('process');

const { version } = require('./package.json');

const flip = f => x => y => f(y)(x);

const desiredRatio = 16 / 9;
const desiredRatioFlipped = 1 / desiredRatio;

const cmdParser = yargs(hideBin(process.argv))
    .usage('Usage:\n$0 -i input.png -o output.png\n$0 img.png # output will be saved to img_twres.png')

    .string('input')
    .alias('i', 'input')
    .describe('i', 'Input file')

    .string('output')
    .alias('o', 'output')
    .describe('o', 'Output file')

    .number('b')
    .alias('b', 'blur')
    .describe('b', 'Blur intensity')
    .default('b', 5)

    .number('quality')
    .alias('q', 'quality')
    .describe('q', 'Quality of output JPEG (0-100)')
    .default('q', 90)

    .boolean('verbose')
    .describe('v', 'Print debug logs')
    .alias('v', 'verbose')

    .help('h')
    .alias('h', 'help')

    .version()

    .epilog(`twres v${version} | monnef (c) 2022 under GPLv3`);

const processCmdArgs = async() => await cmdParser.parse();

const genOutputName = inputName => {
    const p = path.parse(inputName);
    return path.join(p.dir, p.name + '_twres' + p.ext);
};

const main = async() => {
    const rawArgv = await processCmdArgs();
    const debugLog = (...args) => { if (rawArgv.verbose) console.log(...args); };
    debugLog({ rawArgv });
    const argv = cloneDeep(rawArgv);
    if (!argv.input && !isEmpty(argv._)) argv.input = head(argv._);
    if (!argv.output && argv.input) argv.output = genOutputName(argv.input);
    debugLog({ argv });

    if (!argv.input) {
        console.error('Missing input file name.\n\n' + await cmdParser.getHelp());
        exit(1);
    }

    const img = await Jimp.read(argv.input);

    const size = [img.getWidth(), img.getHeight()];
    const ratio = size[0] / size[1]
    const targetRes = ratio > desiredRatio ?
        [size[0], size[0] * desiredRatioFlipped] :
        [size[1] * desiredRatio, size[1]];
    const targetResRatio = targetRes[0] / targetRes[1];
    debugLog({ size, targetRes, ratio, desiredRatio, targetResRatio });

    const bgImg = img
        .clone()
        .resize(...targetRes, Jimp.RESIZE_BICUBIC);
    if (argv.blur > 0) bgImg.blur(argv.blur);
    if (argv.quality) bgImg.quality(argv.quality);

    const targetPos = pipe(
        size,
        zip(targetRes),
        map(flow(uncurryTuple(subtract), flip(divide)(2))),
    );
    debugLog({ targetPos });

    bgImg.composite(img, ...targetPos);
    bgImg.writeAsync(argv.output);
}
main();
