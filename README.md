# twres

Converts an image for use on Twitter (resize to 16:9 and add blurry background).

![](example.png)

```sh
$ twres -h
Usage:
twres -i input.png -o output.png
twres img.png # output will be saved to img_twres.png

Options:
  -i, --input    Input file                                             [string]
  -o, --output   Output file                                            [string]
  -b, --blur     Blur intensity                            [number] [default: 5]
  -q, --quality  Quality of output JPEG (0-100)           [number] [default: 90]
  -v, --verbose  Print debug logs                                      [boolean]
  -h, --help     Show help                                             [boolean]
      --version  Show version number                                   [boolean]

twres v1.2.0 | monnef (c) 2022 under GPLv3
```

# Installation

0) have [nodejs](https://nodejs.org) installed (tested with version in file `/.node-version`)

## From NPM
```sh
$ npm i -g twres
...
$ twres --version
1.2.0
```

## Manual
1) clone this repository
2) navigate to the directory
3) install dependencies  
   ```sh
   $ npm i
   ```
4) create link, so it can be run as `twres` from anywhere  
   ```sh
   $ npm link
   ```
5) profit  
   ```sh
   $ twres --version
   1.2.0
   ```

# License
**GPLv3**
